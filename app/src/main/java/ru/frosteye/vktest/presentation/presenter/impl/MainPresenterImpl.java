package ru.frosteye.vktest.presentation.presenter.impl;

import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKWallPostResult;

import java.util.Calendar;

import javax.inject.Inject;

import ru.frosteye.ovsa.data.storage.ResourceHelper;
import ru.frosteye.vktest.R;
import ru.frosteye.vktest.presentation.view.contract.MainView;

import ru.frosteye.ovsa.presentation.presenter.BasePresenter;
import ru.frosteye.vktest.presentation.presenter.contract.MainPresenter;

public class MainPresenterImpl extends BasePresenter<MainView> implements MainPresenter {

    @Inject
    public MainPresenterImpl() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onPostReady(String text, Calendar calendar) {
        enableControls(false);
        VKParameters parameters = new VKParameters();
        parameters.put(VKApiConst.MESSAGE, text);
        if(calendar != null) {
            parameters.put(VKApiConst.PUBLISH_DATE, calendar.getTimeInMillis() / 1000);
        }
        VKRequest post = VKApi.wall().post(parameters);
        post.setModelClass(VKWallPostResult.class);
        post.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                enableControls(true);
                showMessage(ResourceHelper.getString(R.string.added));
                view.clear();
            }
            @Override
            public void onError(VKError error) {
                enableControls(true);
                showMessage(error.apiError.errorMessage);
            }
        });
    }
}
