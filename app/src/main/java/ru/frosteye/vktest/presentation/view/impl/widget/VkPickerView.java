package ru.frosteye.vktest.presentation.view.impl.widget;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.frosteye.ovsa.presentation.view.widget.BaseLinearLayout;
import ru.frosteye.ovsa.tool.DateTools;
import ru.frosteye.vktest.R;

/**
 * Created by oleg on 02.10.17.
 */

public class VkPickerView extends BaseLinearLayout {

    @BindView(R.id.view_picker_date) TextView date;
    @BindView(R.id.view_picker_time) TextView time;

    private Calendar calendar;

    public interface Listener {
        void onResult(Calendar calendar);
    }

    public VkPickerView(Context context) {
        super(context);
    }

    public VkPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VkPickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public VkPickerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void prepareView() {
        ButterKnife.bind(this);
        drop();
        time.setOnClickListener(v -> DateTools.showTimeDialogWithButtons(getContext(), new DateTools.TimePickerCallback() {
            @Override
            public void onTimeSelected(String tm) {
            }

            @Override
            public void onRaw(int hour, int minute) {
                calendar.set(Calendar.HOUR_OF_DAY, hour);
                calendar.set(Calendar.MINUTE, minute);
                fill();
            }
        }));
        date.setOnClickListener(v -> {
            DateTools.showDateDialogWithButtons(getContext(), date1 -> {
                calendar.set(Calendar.YEAR, 1900 + date1.getYear());
                calendar.set(Calendar.MONTH, date1.getMonth());
                calendar.set(Calendar.DAY_OF_MONTH, date1.getDate());
                fill();
            }, calendar, true);
        });
    }

    public void drop() {
        calendar = Calendar.getInstance();
        fill();
    }

    private void fill() {
        date.setText(DateTools.formatPrettyDate(calendar.getTime()));
        time.setText(DateTools.formatTime(calendar.getTime()));
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
        fill();
    }

    public Calendar getResult() {
        return calendar;
    }

    public static void showDialog(Activity activity, Listener listener, Calendar initial) {
        VkPickerView view = ((VkPickerView) LayoutInflater.from(activity).inflate(R.layout.view_pick_time, null));
        view.setCalendar(initial);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(view);
        builder.setTitle(R.string.post);
        builder.setPositiveButton(R.string.ok, (dialog, which) -> listener.onResult(view.getResult()));
        builder.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.cancel());
        builder.setNeutralButton(R.string.drop, (dialog, which) -> view.drop());

        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(arg0 -> {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.RED);
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(activity.getResources().getColor(R.color.text_link));
            dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(activity.getResources().getColor(R.color.text_link));
            dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(v -> view.drop());
        });
        dialog.show();
    }
}
