package ru.frosteye.vktest.presentation.view.contract;

import ru.frosteye.ovsa.presentation.view.BasicView;

public interface MainView extends BasicView {
    void clear();
}
