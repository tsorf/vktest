package ru.frosteye.vktest.presentation.presenter.contract;

import java.util.Calendar;

import ru.frosteye.vktest.presentation.view.contract.MainView;

import ru.frosteye.ovsa.presentation.presenter.LivePresenter;

public interface MainPresenter extends LivePresenter<MainView> {
    void onPostReady(String text, Calendar calendar);
}
