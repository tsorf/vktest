package ru.frosteye.vktest.presentation.view.impl.activity;

import javax.inject.Inject;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.Calendar;

import butterknife.BindView;
import ru.frosteye.ovsa.tool.TextTools;
import ru.frosteye.vktest.app.di.component.PresenterComponent;
import ru.frosteye.vktest.presentation.presenter.contract.MainPresenter;
import ru.frosteye.vktest.presentation.view.contract.MainView;
import ru.frosteye.ovsa.presentation.presenter.LivePresenter;
import ru.frosteye.ovsa.presentation.view.activity.PresenterActivity;
import ru.frosteye.vktest.R;
import ru.frosteye.vktest.presentation.view.impl.widget.VkPickerView;

public class MainActivity extends BaseActivity implements MainView {

    @BindView(R.id.activity_main_picker) ImageButton picker;
    @BindView(R.id.common_toolbar) Toolbar toolbar;
    @BindView(R.id.activity_main_field) EditText field;

    @Inject MainPresenter presenter;

    private Calendar calendar;
    private boolean enabled = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void enableControls(boolean enabled, int code) {
        this.enabled = enabled;
        hideKeyboard();
        field.setEnabled(enabled);
        invalidateOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.findItem(R.id.action_done).setEnabled(enabled);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_done && !TextTools.isTrimmedEmpty(field)) {
            presenter.onPostReady(TextTools.extractTrimmed(field), calendar);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initView() {
        setSupportActionBar(toolbar);
        picker.setOnClickListener(v -> VkPickerView.showDialog(this, calendar ->
                this.calendar = calendar, calendar != null ? calendar : Calendar.getInstance()));
    }

    @Override
    protected void attachPresenter() {
        presenter.onAttach(this);
    }

    @Override
    protected LivePresenter<?> getPresenter() {
        return presenter;
    }

    @Override
    protected void inject(PresenterComponent component) {
        component.inject(this);
    }

    @Override
    public void clear() {
        field.setText(null);
        calendar = null;
    }
}
