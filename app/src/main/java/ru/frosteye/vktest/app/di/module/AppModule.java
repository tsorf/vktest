package ru.frosteye.vktest.app.di.module;

import com.vk.sdk.VKSdk;

import ru.frosteye.vktest.app.environment.VKTest;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.frosteye.ovsa.di.module.BaseAppModule;

import static ru.frosteye.ovsa.data.storage.ResourceHelper.*;


@Module
public class AppModule extends BaseAppModule<VKTest> {

    public AppModule(VKTest context) {
        super(context);

        VKSdk.initialize(context);
    }
}
