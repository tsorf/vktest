package ru.frosteye.vktest.app.di.component;

import ru.frosteye.vktest.app.di.module.AppModule;
import ru.frosteye.vktest.app.di.module.PresenterModule;
import ru.frosteye.vktest.app.di.module.RepoModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {AppModule.class, RepoModule.class})
@Singleton
public interface AppComponent {
    PresenterComponent plus(PresenterModule module);
}