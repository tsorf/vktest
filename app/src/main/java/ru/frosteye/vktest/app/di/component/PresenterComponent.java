package ru.frosteye.vktest.app.di.component;

import ru.frosteye.vktest.app.di.module.PresenterModule;
import ru.frosteye.vktest.app.di.scope.PresenterScope;
import ru.frosteye.vktest.presentation.view.impl.activity.BaseActivity;
import ru.frosteye.vktest.presentation.view.impl.activity.MainActivity;
import ru.frosteye.vktest.presentation.view.impl.fragment.BaseFragment;

import dagger.Subcomponent;

@PresenterScope
@Subcomponent(modules = PresenterModule.class)
public interface PresenterComponent {
    void inject(BaseFragment baseFragment);

    void inject(BaseActivity baseActivity);
    void inject(MainActivity activity);
}
