package ru.frosteye.vktest.app.environment;

import android.app.Application;

import com.vk.sdk.VKSdk;

import ru.frosteye.vktest.app.di.component.AppComponent;
import ru.frosteye.vktest.app.di.component.DaggerAppComponent;
import ru.frosteye.vktest.app.di.module.AppModule;


public class VKTest extends Application {

    private static AppComponent appComponent;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }
}
